package org.adfemg.audits.trinidadconfig;

import oracle.jdeveloper.audit.analyzer.AuditContext;
import oracle.jdeveloper.audit.analyzer.Rule;
import oracle.jdeveloper.audit.extension.ExtensionResource;

import org.adfemg.audits.base.XmlAnalyzer;
import org.adfemg.audits.utils.XmlUtils;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


/**
 * Analyzer class for the trinidad-config.xml
 * Currently there is only one rule defined on the Trinidad-config.xml.
 */
public class TrinidadConfigAnalyzer extends XmlAnalyzer {
    /**
     * [ADFcg1-01002] � Consider animation-enabled=false for browser performance - Consider the faster
     * browser performance turning ADF Faces RC animations off. At minimum give each use the ability to
     * determine this themselves.
     */
    @ExtensionResource("org.adfemg.audits.trinidadconfig.animation-enabled")
    private Rule ANIMATION_ENABLED;

    public TrinidadConfigAnalyzer() {
        super("trinidad-config");
    }

    /**
     * Check if we're the animation-enabled and check if we're true.
     */
    public void exit(AuditContext context, Element element) {
        if ("trinidad-config".equals(element.getNodeName())) {
            //in trinidad, check if we have a animation-enabled child.
            NodeList nodeList = element.getChildNodes();
            if (!XmlUtils.doesNodeExist(nodeList, "animation-enabled")) {
                context.report(ANIMATION_ENABLED);
            }
        }

        if ("animation-enabled".equals(element.getNodeName()) && "true".equalsIgnoreCase(element.getTextContent())) {
            context.report(ANIMATION_ENABLED);
        }
    }

}
