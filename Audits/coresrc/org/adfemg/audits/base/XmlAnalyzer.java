package org.adfemg.audits.base;

import oracle.jdeveloper.audit.analyzer.Analyzer;
import oracle.jdeveloper.audit.analyzer.AuditContext;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XmlAnalyzer extends Analyzer {

    private final String docElemName;

    public XmlAnalyzer(final String docElemName) {
        super();
        this.docElemName = docElemName;
    }

    protected boolean isAuditable(Document document) {
        Element docElem = document != null ? document.getDocumentElement() : null;
        String name = docElem != null ? docElem.getNodeName() : null;
        return (this.docElemName != null && this.docElemName.equals(name));
    }

    public void enter(AuditContext context, Document document) {
        if (!isAuditable(document)) {
            setEnabled(false);
        }
    }

}
