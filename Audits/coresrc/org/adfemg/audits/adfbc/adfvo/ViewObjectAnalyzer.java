package org.adfemg.audits.adfbc.adfvo;

import oracle.jdeveloper.audit.analyzer.AuditContext;
import oracle.jdeveloper.audit.analyzer.Rule;
import oracle.jdeveloper.audit.extension.ExtensionResource;

import org.adfemg.audits.base.XmlAnalyzer;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;


/**
 * Analyzer class for ViewObjects.
 */
public class ViewObjectAnalyzer extends XmlAnalyzer {

    /**
     * [ADFcg1-02041] – Avoid read only view objects for speed benefits alone - Older ADF manuals
     * recommended using read only view objects as there was a minor speed benefit. Since JDeveloper 11g this
     * is no longer true as the caching mechanism has been improved. Today read only view objects should be
     * reserved for complex queries such as those with set operations (e.g. UNION, MINUS, INTERSECT)
     * and connect by queries. All other view objects should be based on entity objects. If the view object
     * needs to be read only, unselect the "Updateable" property when the entity object is mapped to view objects.
     */
    @ExtensionResource("org.adfemg.audits.adfbc.adfvo.read-only-vo")
    private Rule READ_ONLY_VO;

    /**
     * [ADFcg1-02043] – Rarely use view object expert mode - Rarely use expert mode for view objects. If
     * the query you wish to execute can be implemented by modifying other properties of the view object over
     * expert mode this should be your preference. For example SQL functions on attributes can be applied at
     * the attribute level.
     */
    @ExtensionResource("org.adfemg.audits.adfbc.adfvo.expert-mode-vo")
    private Rule EXPERT_MODE_VO;

    /**
     * [ADFcg1-02054] – Never use view object select * - Never use SELECT * for a view object query.
     * Always specify all columns in the query.
     */
    @ExtensionResource("org.adfemg.audits.adfbc.adfvo.select-star-vo")
    private Rule SELECT_STAR_VO;

    /**
     * [ADFcg1-02053] – Use view criteria over editing view object where clauses - Rather than
     * customizing a view objects where clause which makes the use of the view object less flexible, use a view criteria instead.
     */
    @ExtensionResource("org.adfemg.audits.adfbc.adfvo.where-clause-vo")
    private Rule WHERE_CLAUSE_VO;

    /**
     * [ADFcg1-02061] – Do not generate a ViewDefImpl class - Do not generate the ViewDefImpl class for your
     * view object unless needed.
     */
    @ExtensionResource("org.adfemg.audits.adfbc.adfvo.def-class-vo")
    private Rule DEF_CLASS_VO;

    /**
     * [ADFcg1-02055] – Avoid hardcoding view object queries with literals
     * Always use bind variables.
     */
    @ExtensionResource("org.adfemg.audits.adfbc.adfvo.literal-value-vo")
    private Rule LITERAL_VALUE_VO;

    public ViewObjectAnalyzer() {
        super("ViewObject");
    }

    /**
     * Check if the SQLQuery contains a select * statement and
     * Check if the VO is in expert mode.
     */
    public void exit(AuditContext context, Element element) {
        if ("SQLQuery".equals(element.getNodeName())) {
            if (element.getTextContent().contains("select *") || element.getTextContent().contains("SELECT *")) {
                context.report(SELECT_STAR_VO);
            }
            //Check for where clause in read only VO with custom query.
            if (element.getTextContent().contains("where") || element.getTextContent().contains("WHERE")) {
                context.report(WHERE_CLAUSE_VO);
            }
        }

        if ("Attr".equalsIgnoreCase(element.getNodeName()) &&
            "_isExpertMode".equalsIgnoreCase(element.getAttribute("Name")) &&
            "true".equalsIgnoreCase(element.getAttribute("Value"))) {
            context.report(EXPERT_MODE_VO);
        }

        if ("ViewCriteriaItem".equals(element.getNodeName())) {
            if (checkLiteralValue(element)) {
                context.report(LITERAL_VALUE_VO);
            }
        }
    }

    /**
     * Check if the VO is readonly.
     */
    public void exit(AuditContext context, Attr attr) {
        if ("CustomQuery".equals(attr.getNodeName()) && "true".equalsIgnoreCase(attr.getValue())) {
            context.report(READ_ONLY_VO);
        }
        //Check if VO has Where Clause and report voilation.
        if ("Where".equals(attr.getNodeName()) && attr.getValue() != null) {
            context.report(WHERE_CLAUSE_VO);
        }
        //Check if VO has ViewDefinationImpl class and report voilation.
        if ("DefClass".equals(attr.getNodeName()) && attr.getValue() != null) {
            context.report(DEF_CLASS_VO);
        }
    }

    /**
     * This method parses the task flow file and checks for bind variable usage in view criteria.
     * Method returns 'true' if there is usage of literal value with hard code value.
     * @param element
     * @return boolean
     */
    private boolean checkLiteralValue(Element element) {
        NamedNodeMap nodeMap = element.getAttributes();
        for (int i = 0; i < nodeMap.getLength(); i++) {
            Node nodeInner = nodeMap.item(i);
            if ("IsBindVarValue".equals(nodeInner.getNodeName())) {
                return false;
            }
        }
        return true;
    }
}
