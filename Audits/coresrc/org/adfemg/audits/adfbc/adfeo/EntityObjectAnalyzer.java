package org.adfemg.audits.adfbc.adfeo;


import oracle.ide.model.Workspaces;

import oracle.javatools.parser.java.v2.model.SourceBlock;
import oracle.javatools.parser.java.v2.model.SourceFile;

import oracle.jdeveloper.audit.analyzer.AuditContext;
import oracle.jdeveloper.audit.analyzer.Rule;
import oracle.jdeveloper.audit.extension.ExtensionResource;

import org.adfemg.audits.base.XmlAnalyzer;
import org.adfemg.audits.utils.XmlUtils;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;


/**
 * Analyzer class for EntityObjects.
 */
public class EntityObjectAnalyzer extends XmlAnalyzer {
    /**
     * [ADFcg1-02027] - Don't use entity object key ROWID - By default if ADF BC doesn't find a primary key at
     * design time for an entity object's relating database table, it will generate an additional attribute mapping
     * to ROWID.  This will continue to work unless an Oracle database Real Application Cluster (RAC) is used where
     * across database partitions different ROWIDs can be returned for the same row.  Therefore attempt to avoid using
     * the ROWID attribute and instead select a combination of existing attributes as the primary key
     */
    @ExtensionResource("org.adfemg.audits.adfbc.adfeo.rowid-pk-eo")
    private Rule ROWID_PK_EO;

    /**
     * [ADFcg2-02029] � Use entity object attribute change indicators - When an entity object is updated concurrently by more than
     * one user and one of those users loses affinity with their application module during the update, it is possible to experience
     * data corruption if a change indicator has not been defined for the entity object. To avoid this type of scenario, define a
     * change indicator attribute for all entity objects.
     */
    @ExtensionResource("org.adfemg.audits.adfbc.adfeo.change-indicator-eo")
    private Rule CHANGE_INDICATOR_EO;

    /**
     * [ADFcg1-02024] Do not access an application module from an entity object - entity objects should not access
     * application modules as this makes the entity object only useable within that application module.
     */
    @ExtensionResource("org.adfemg.audits.adfbc.adfeo.am-reference-eo")
    private Rule AM_REFRENCE_EO;

    /**
     * Boolean flag to check if ChangeIndicator Found in Entity Object.
     */
    private AuditContext.Key changeIndFound;

    public EntityObjectAnalyzer() {
        super("Entity");
    }

    public void enter(AuditContext context, Workspaces workspaces) {
        changeIndFound = context.key("changeIndFound");
    }

    /**
     * Check if we're in a EntityObject file.
     * @param context the AuditContext.
     * @param document the document.
     */
    public void enter(AuditContext context, Document document) {
        super.enter(context, document);
        context.setAttribute(changeIndFound, false);
    }

    /**
     * Check if we're in EntityImpl java file.
     * @param context
     * @param sourceClass
     */
    public void enter(AuditContext context, SourceFile sourceClass) {
        if (sourceClass.getSourcePrimaryClass().getSuperclass() != null && !"EntityImpl".equals(sourceClass.getSourcePrimaryClass().getSuperclass().getName())) {
            setEnabled(false);
        }
    }

    /**
     * Check if RowId is used as primary key and report voilation.
     * @param context
     * @param element
     */
    public void exit(AuditContext context, Element element) {
        if ("Attribute".equals(element.getNodeName())) {
            NamedNodeMap nodeMap = element.getAttributes();
            Node node = XmlUtils.findFirstAttributeInList(nodeMap, "Name");
            if (node != null && "RowID".equals(node.getNodeValue())) {
                if (XmlUtils.doesAttributExist(nodeMap, "PrimaryKey")) {
                    context.report(ROWID_PK_EO);
                }
            }
        }
        //Check if any attribute has been used as ChangeIndicator.
        if (!(Boolean) context.getAttribute(changeIndFound) && "Attribute".equals(element.getNodeName())) {
            NamedNodeMap nodeMap = element.getAttributes();
            Node node = XmlUtils.findFirstAttributeInList(nodeMap, "Name");
            if (XmlUtils.doesAttributExist(nodeMap, "ChangeIndicator")) {
                context.setAttribute(changeIndFound, true);
            }
        }

        if ("Entity".equals(element.getNodeName())) {
            if (!(Boolean) context.getAttribute(changeIndFound)) {
                context.report(CHANGE_INDICATOR_EO);
            }
        }
    }

    /**
     *
     * @param context
     * @param attr
     */
    public void exit(AuditContext context, Attr attr) {

    }

    /**
     * Checks if there is refrence to Application Module in EntityImpl class.
     * @param context
     * @param stmt
     */
    public void exit(AuditContext context, SourceBlock stmt) {
        String methodText = stmt.getText();
        if (methodText.contains("ApplicationModule") || methodText.contains("AppModule")) {
            context.report(AM_REFRENCE_EO);
        }
    }

}
