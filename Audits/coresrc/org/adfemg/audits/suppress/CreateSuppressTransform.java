package org.adfemg.audits.suppress;

import oracle.jdeveloper.audit.service.HasSuppressionName;
import oracle.jdeveloper.audit.transform.Transform;
import oracle.jdeveloper.audit.xml.XmlTransformAdapter;
import oracle.jdeveloper.audit.xml.XmlTransformContext;

import org.w3c.dom.Attr;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Transform to add XML Comment Node to suppress and audit rule in a XML
 * document. Is typically invoked by the end user from the gutter in the
 * JDev editor.
 */
public class CreateSuppressTransform extends Transform implements HasSuppressionName {

    private String suppressRuleName;

    /**
     * Default no-arg constructor.
     */
    public CreateSuppressTransform() {
        super(new XmlTransformAdapter());
    }

    /**
     * Sets the name of the audit rule to suppress.
     * @param suppressRuleName name of the rule to suppress
     */
    @Override
    public void setSuppressionName(String suppressRuleName) {
        this.suppressRuleName = suppressRuleName;
    }

    /**
     * Gets the name of the audit rule to suppress.
     * @return name of the rule to suppress
     */
    @Override
    public String getSuppressionName() {
        return suppressRuleName;
    }

    /**
     * Create a XML comment to suppress the audit rule in a XML document. This is
     * invoked by the audit framework when the user selects this from the gutter
     * in the editor or the rule violation popup.
     * @param context Context which this Transform
     * @param node XML Node with a violation that needs to be suppressed
     */
    public void apply(@SuppressWarnings("unused") XmlTransformContext context, Node node) {
        Element target = findTarget(node);
        if (target == null) {
            return;
        }
        // create new xml comment Node
        Document document = target.getOwnerDocument();
        Comment comment =
            document.createComment(" " + SuppressAnalyzer.COMMENT_PREFIX + ": " + getSuppressionName() + " ");
        // insert comment before target-element
        target.getParentNode().insertBefore(comment, target);
    }

    /**
     * Finds the XML Element that should be the target to suppress a rule on the
     * supplied Node.
     * @param node Node to start searching
     * @return Node itself if it is an Element, or the closest parent of node that is
     *         an Element, or <code>null</code> if no Element was found
     */
    private Element findTarget(Node node) {
        while (node != null && !(node instanceof Element)) {
            node = node instanceof Attr ? ((Attr) node).getOwnerElement() : node.getParentNode();
        }
        return (Element) node;
    }

}
