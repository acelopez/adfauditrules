package org.adfemg.audits.suppress;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.jdeveloper.audit.analyzer.Analyzer;
import oracle.jdeveloper.audit.analyzer.AuditContext;
import oracle.jdeveloper.audit.analyzer.SuppressionScheme;
import oracle.jdeveloper.audit.extension.ExtensionResource;

import org.w3c.dom.Comment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Analyzer to detect SuppressWarning comments in XML files and to activate the
 * org.adfemg.audits.suppress.suppress-xml-warnings SuppressionScheme on the first
 * element after this comment to suppress any violations for the rule specified
 * in the comment.
 * @author Wilfred van der Deijl, Red Heap
 */
public class SuppressAnalyzer extends Analyzer {

    /** keyword in XML comments to suppress a rule violation */
    public static final String COMMENT_PREFIX = "SuppressWarning";

    // dependency injection for SuppressionScheme
    @ExtensionResource("org.adfemg.audits.suppress.suppress-xml-warnings")
    private SuppressionScheme SUPPRESS_XML_WARNINGS;

    // regex that a XML comment should match to trigger suppressions:
    private static final String REGEX_BEGIN = "^";
    private static final String REGEX_END = "$";
    private static final String REGEX_OPTIONAL_WHITESPACE = "\\s*";
    private static final String REGEX_ANYTHING = ".*";
    private static final Pattern PATTERN =
        Pattern.compile(REGEX_BEGIN + REGEX_OPTIONAL_WHITESPACE + Pattern.quote(COMMENT_PREFIX) +
                        REGEX_OPTIONAL_WHITESPACE + ":" + REGEX_OPTIONAL_WHITESPACE + "(\\S+)" + REGEX_ANYTHING +
                        REGEX_END);

    /**
     * Invoked during audit traversel when a XML Comment Node is encountered. Used
     * to detect if this comment is used to suppress a rule. If so, a suppression
     * is activated on the first XML Element Node after this Comment Node
     * @param context current AuditContext
     * @param comment XML Comment Node being audited
     */
    public void enter(AuditContext context, Comment comment) {
        String txt = comment.getData();
        if (txt == null) {
            return; // huh? comment without text??
        }
        Matcher matcher = PATTERN.matcher(txt);
        if (matcher.matches()) {
            String rule = matcher.group(1);
            Element target = findTarget(comment);
            if (target == null) {
                return; // no element found after the comment
            }
            // suppress rule unlimited nr of times (0) on target element (and all children)
            context.report(SUPPRESS_XML_WARNINGS, rule, target);            
        }
    }

    /**
     * Find the first XML Element sibling to a XML Comment Node
     * @param comment XML Comment Node to start searching
     * @return first sibling of the Comment Node that is an Element, or <code>null</code>
     *         if none found
     */
    private Element findTarget(Comment comment) {
        Node retval = comment;
        while (retval != null && !(retval instanceof Element)) {
            retval = retval.getNextSibling();
        }
        return (Element) retval;
    }

}
