package org.adfemg.audits.utils;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class AuditUtils {
    //FIXME: Fill out all the group components.
    //javax.faces.component Interface NamingContainer
    private static final String[] CONTAINER_COMPONENTS = {
        "declarativeComponent", "navigationPane", "panelCollection", "query", "quickQuery", "region", "pageTemplate",
        "subform", "table", "train", "hierarchyViewer", "map", "pivotTable", "projectGantt"
    };

    public static boolean isGroupComponent(String componentName) {
        for (String gc : CONTAINER_COMPONENTS) {
            if (gc.equalsIgnoreCase(componentName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * This method loads task flow template at location templatePathName. Method returns true if task flow template
     * consist exception handler else return false.
     * @param templateName
     * @param url
     * @return true if task flow template consist exception handler else return false.
     */
    public static boolean handlerExistInTFTemplate(final String templateName, String url) {
        String pathUrl = url.substring(0, url.indexOf("public_html"));
        String templatePathName = pathUrl + "public_html" + templateName;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            //Load and Parse the XML document
            Document document = builder.parse(new File(templatePathName));
            NodeList nodeList = document.getDocumentElement().getChildNodes();

            Node node = XmlUtils.findFirstNodeInList(nodeList, "task-flow-template");
            if (node != null) {
                if (XmlUtils.doesNodeExist(node.getChildNodes(), "exception-handler")) {
                    return true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
