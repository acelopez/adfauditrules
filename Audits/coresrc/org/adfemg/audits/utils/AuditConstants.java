package org.adfemg.audits.utils;

public class AuditConstants {
    /**
     * af namespace declaration.
     */
    public static final String NS_AF = "http://xmlns.oracle.com/adf/faces/rich";

    /**
     * f namespace declaration.
     */
    public static final String NS_F = "http://java.sun.com/jsf/core";

    /**
     * jsppage namespace declaration.
     */
    public static final String NS_JSPPAGE = "http://java.sun.com/JSP/Page";

    /**
     * Namespace declaration for the placeholder DC ns.
     */
    public static final String NS_PLACEHOLDER_DC = "http://xmlns.oracle.com/placeholder";

    /**
     * Maximum lenght for an ID of a naming container.
     */
    public static final int MAX_CONTAINER_LENGTH = 4;

    /**
     * Maximum lenght for an ID.
     */
    public static final int MAX_ID_LENGTH = 10;

    /**
     * Maximum number of regions in a page.
     */
    public static final int MAX_REGIONS_IN_PAGE = 10;
}
