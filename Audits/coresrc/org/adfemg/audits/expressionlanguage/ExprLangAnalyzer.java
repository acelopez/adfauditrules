package org.adfemg.audits.expressionlanguage;

import oracle.jdeveloper.audit.analyzer.Analyzer;
import oracle.jdeveloper.audit.analyzer.AuditContext;
import oracle.jdeveloper.audit.analyzer.Rule;
import oracle.jdeveloper.audit.extension.ExtensionResource;

import org.w3c.dom.Attr;


/**
 * Analyzer class for Expression Language.
 */
public class ExprLangAnalyzer extends Analyzer {

    /**
     * [ADFcg1-03020] - Do not prefix EL references to managed beans in request, session or
     * application scope with the *Scope prefix - Although it seems to be a good thing to be explicit about
     * the scope that you expect such an object to be in, doing so will bypass the JSF creation routines if the
     * bean does not already exist. This is not an issue for the ADF specific scopes.
     */
    @ExtensionResource("org.adfemg.audits.expressionlanguage.bean-scope-prefix")
    private Rule BEAN_SCOPE_PREFIX;

    /**
     * [ADFcg1-03028] - Don’t reference #{data} - The #{data} expression root is not guaranteed to provide
     * access to the required data. Success will depend on the state of the cache. If data is needed in a particular
     * view / method then create an explicit binding in that scope.
     */
    @ExtensionResource("org.adfemg.audits.expressionlanguage.data-expr-reference")
    private Rule DATA_EXPR_REFERENCE;

    /**
     * Exit an attribute, check if this element is inside an iterator.
     * If so, check if we're an cacheResult attribute and check the value.
     * Report the results back to the Editor.
     */
    public void exit(AuditContext context, Attr attr) {
        String value = attr.getValue();
        if (isElExpression(value)) { //We're in an EL expression
            if (stringContainsScope(value)) {
                context.report(BEAN_SCOPE_PREFIX);
            }
            if (stringContainsData(value)) {
                context.report(DATA_EXPR_REFERENCE);
            }
        }
    }

    /**
     * Check needs to be improved, but it's a starting point :).
     */
    private boolean isElExpression(String expression) {
        if (expression.contains("#{") && expression.contains("}")) {
            //TODO: Check on escaped hash.
            return true;
        }
        return false;
    }

    /**
     * Check if the String contains one of the scopes.
     * @param expression the EL expression to check.
     * @return
     */
    private boolean stringContainsScope(String expression) {
        if (expression.contains("requestScope.") || expression.contains("sessionScope.") ||
            expression.contains("applicationScope.")) {
            return true;
        }
        return false;
    }

    /**
     * Checks if the string contains the 'data.'.
     * @param expression the EL expression to check.
     * @return
     */
    private boolean stringContainsData(String expression) {
        if (expression.contains("data.")) {
            return true;
        }
        return false;
    }
}
