package org.adfemg.audits.adfviewcontroller.problematic_components;

import oracle.jdeveloper.audit.analyzer.Analyzer;
import oracle.jdeveloper.audit.analyzer.AuditContext;
import oracle.jdeveloper.audit.analyzer.Rule;
import oracle.jdeveloper.audit.extension.ExtensionResource;

import org.adfemg.audits.utils.AuditConstants;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * Analyzer class for PageComponents.
 */

public class ProblemComponentsAnalyzer extends Analyzer {

    /**
     * [ADFcg1-03045] - Avoid using the af:inlineFrame tag - Inline frames may introduce problems both
     * with geometry management and performance (especially on mobile browsers).
     */
    @ExtensionResource("org.adfemg.audits.adfviewcontroller.problematic_components.avoid-inline-frame")
    private Rule AVOID_INLINE_FRAME;

    /**
     * [ADFcg1-03046] - Avoid using the f:verbatim tag - The use of verbatim is often linked to other known
     * issues such as the use of embedded HTML and generally should be avoided. For scenarios such as
     * embedding applets or other embedded technologies, consider creating a custom component wrapper.
     */
    @ExtensionResource("org.adfemg.audits.adfviewcontroller.problematic_components.avoid-verbatim-tag")
    private Rule AVOID_VERBATIM_TAG;

    /**
     * [ADFcg1-03047] - Do not use the jsp:include tag - As well as tying the page to a particular page
     * assembly technology, JSP Scriptlets do not fit in with any of the lifecycle aspects of ADF or JSF. Use
     * method activities in ADF Task Flows or lifecycle annotations (in JSF 2.0) for these purposes. Pages with
     * JSP includes will not be upgradable to use Facelets. Instead use the af:declarativeComponent tag.
     */
    @ExtensionResource("org.adfemg.audits.adfviewcontroller.problematic_components.avoid-include-tag")
    private Rule AVOID_INCLUDE_TAG;

    /**
     * Check if we're in a page or a page fragment file.
     * @param context the AuditContext.
     * @param document the document.
     */
    public void enter(AuditContext context, Document document) {
        String fileName = context.getUrl().getFile();
        if (!fileName.matches(".*.jsf") && !fileName.matches(".*.jspx") && !fileName.matches(".*.jsff")) {
            setEnabled(false);
        }
    }

    /**
     * Check if the elements to avoid are included in the page.
     */
    public void exit(AuditContext context, Element element) {
        if ("include".equals(element.getLocalName()) && AuditConstants.NS_JSPPAGE.equals(element.getNamespaceURI())) {
            context.report(AVOID_INCLUDE_TAG);
        }
        if ("verbatim".equals(element.getLocalName()) && AuditConstants.NS_F.equals(element.getNamespaceURI())) {
            context.report(AVOID_VERBATIM_TAG);
        }
        if ("inlineFrame".equals(element.getLocalName()) && AuditConstants.NS_AF.equals(element.getNamespaceURI())) {
            context.report(AVOID_INLINE_FRAME);
        }
    }
}
