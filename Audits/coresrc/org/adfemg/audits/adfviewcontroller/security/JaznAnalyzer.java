package org.adfemg.audits.adfviewcontroller.security;

import oracle.jdeveloper.audit.analyzer.AuditContext;
import oracle.jdeveloper.audit.analyzer.Rule;
import oracle.jdeveloper.audit.extension.ExtensionResource;

import org.adfemg.audits.base.XmlAnalyzer;
import org.adfemg.audits.utils.XmlUtils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * Analyzer class for Jazn files.
 */
public class JaznAnalyzer extends XmlAnalyzer {

    /**
     * [ADFcg1-03069] - Don't use the test-all role - The test-all role is only intended for use during
     * development and must be stripped out before security testing begins
     */
    @ExtensionResource("org.adfemg.audits.adfviewcontroller.security.test-all-role")
    private Rule TEST_ALL_ROLE;

    public JaznAnalyzer() {
        super("jazn-data");
    }

    /**
     * Check if the jazn file contains an app-role with the name test-all and the role has members.
     * @param context
     * @param element
     */
    public void exit(AuditContext context, Element element) {
        if ("app-role".equals(element.getNodeName())) {
            NodeList nodeList = element.getChildNodes();

            Node cls = XmlUtils.findFirstNodeInList(nodeList, "class");
            Node name = XmlUtils.findFirstNodeInList(nodeList, "name");
            //get child class & check.
            //get child name and check.
            if (cls != null && name != null &&
                "oracle.security.jps.service.policystore.ApplicationRole".equals(cls.getNodeValue()) &&
                "test-all".equals(name.getNodeValue())) {
                Node members = XmlUtils.findFirstNodeInList(nodeList, "members");
                if (members != null && members.getChildNodes() != null && members.getChildNodes().getLength() > 0) {
                    //get child members and check if there is at least one member
                    context.report(TEST_ALL_ROLE);
                }
            }
        }
    }
}
