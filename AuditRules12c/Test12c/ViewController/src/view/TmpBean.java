package view;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import oracle.adf.view.rich.component.rich.fragment.RichRegion;

@ManagedBean
@RequestScoped
public class TmpBean {
    private RichRegion region;

    public TmpBean() {
    }

    public void setRegion(RichRegion region) {
        this.region = region;
    }

    public RichRegion getRegion() {
        return region;
    }
}
